classdef Camera < handle
	properties (SetAccess = private)
		Name = '';
		Mode = '';
		View;
		Source;
	end
	methods
		function obj = Camera(name,mode)
			obj.Name = name;
			obj.Mode = mode;
			obj.View = videoinput(obj.Name,1,obj.Mode);
			obj.Source = getselectedsource(obj.View);
			preview(obj.View)
		end
		function close(obj)
			delete obj.View;
		end
		function frame = snapFrame(obj)
			frame = getsnapshot(obj.View);
		end
	end
end