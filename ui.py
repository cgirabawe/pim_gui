from PyQt4 import QtCore, QtGui
import sys, time, os
import matlab.engine
import numpy as np



class MainWindow(QtGui.QWidget):
	def __init__(self):
		super(MainWindow,self).__init__()
		self.resize(300,240)
		self.move(500,300)

		#iniate camera
		print "initiating matlab engine ..."
		self.eng = matlab.engine.start_matlab()
		print "starting camera ..."
		self.cam = self.eng.Camera('dcam',self.cameraMode(4))
		print "camera started"

		self.show()

	def cameraMode(self,n=6):
		modes = {
				0:'F7_Y8_1280x1024_mode0',
				1:'F7_Y8_1280x512_mode1',
				2:'F7_Y8_640x1024_mode2',
				3:'F7_Y8_640x512_mode3',
				4:'Y8_1024X768',
				5:'Y8_1280X960',
				6:'Y8_640x480',
				7:'Y8_800x600'
				}
		return modes[n]
	def snapshot(self):
		pass
	def selectROI(self):
		pass
	def moveROI(self):
		pass
	def record(self):
		pass
	def saveImage(self):
		pass
	def closeEvent(self,event):
		self.eng.quit()

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    mainWindow = MainWindow()
    sys.exit(app.exec_())