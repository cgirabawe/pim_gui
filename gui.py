"""
    Programmable illumination microscope application
    Functionalities:
        - [x] open and close video instance
        - [x] snap a single frame from the video instance
        - [x] save a single or multiple frames from the video instance
        - select multiple regions of interest and save selected coordinates
        - project light on selected regions of interest
        - ability to change coordinates and light intensity of regions of interest
"""

import sys, os, time, cv2
import datetime as dt
import numpy as np
try:
    from PyQt4 import QtGui, QtCore
except:
    from PyQt5 import QtGui, QtCore

#from rois import Polygon



class Camera(QtCore.QThread):
    """ https://gist.github.com/TimSC/5670099 """
    def __init__(self):
        super(Camera,self).__init__()
        self.capture = cv2.cv.CaptureFromCAM(0)
        cv2.cv.SetCaptureProperty(self.capture,cv2.cv.CV_CAP_PROP_FRAME_WIDTH,640)
        cv2.cv.SetCaptureProperty(self.capture,cv2.cv.CV_CAP_PROP_FRAME_HEIGHT,480)

    def run(self):
        while 1:
            time.sleep(1/30.)
            self.frame = cv2.cv.QueryFrame(self.capture)
            im = QtGui.QImage(self.frame.tostring(), self.frame.width, self.frame.height, QtGui.QImage.Format_RGB888).rgbSwapped()
            self.emit(QtCore.SIGNAL("webcam_frame(QImage)"),im)

class VideoPlayer(QtGui.QWidget):
    def __init__(self):
        super(VideoPlayer,self).__init__()
        self.resize(720,540)
        self.move(100,20)
        self.setWindowTitle('Video Player')
        
        #video
        self.scene = QtGui.QGraphicsScene(self)
        self.view  = QtGui.QGraphicsView(self.scene)
        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addWidget(self.view)
        self.camera = Camera()
        QtCore.QObject.connect(self.camera, QtCore.SIGNAL("webcam_frame(QImage)"), self.processFrame)
        self.camera.start()

        #populate and deploy the layout
        self.setLayout(self.vbox)
        self.show()
    
    def processFrame(self,im):
        pix = QtGui.QPixmap(im)
        self.scene.clear()
        self.scene.addPixmap(pix)


class MainWindow(QtGui.QWidget):
    def __init__(self):
        super(MainWindow,self).__init__()
        self.resize(1080,550)
        self.move(200,100)
        self.setWindowTitle('PIM App')
        
        self.__validate_files__()
        #variables
        self.num_rois = 20
        self.frame_counter = 0
        self.time_interval = 1
        self.vertices = []
        self.regions = []
        #self.snapped_image = []
        
        #buttons
        self.btn_open_camera = QtGui.QPushButton('OpenCamera')
        self.btn_open_camera.clicked[bool].connect(self.open_camera)
        
        self.btn_add_roi = QtGui.QPushButton('AddROI')
        #self.btn_add_roi.setCheckable(True)
        self.btn_add_roi.clicked[bool].connect(self.add_roi)
        
        self.btn_move_rois = QtGui.QPushButton('MoveROIs')
        
        self.btn_project = QtGui.QPushButton('Project')
        self.btn_project.clicked[bool].connect(self.project)
        
        self.btn_snapshot = QtGui.QPushButton('SnapShot')
        self.btn_snapshot.clicked[bool].connect(self.snapshot)
        
        self.btn_save_image = QtGui.QPushButton('SaveImage')
        self.btn_save_image.clicked[bool].connect(self.save_snapped)
        
        self.btn_record = QtGui.QPushButton('Record')
        self.btn_record.setCheckable(True)
        self.btn_record.clicked[bool].connect(self.record)
        self._record_signal = False
        
        #table
        self.tbl = QtGui.QTableWidget(self.num_rois,3,self)
        self.tbl.setColumnWidth(0,75)
        self.tbl.setColumnWidth(1,75)
        self.tbl.setColumnWidth(2,75)
        self.tbl.setHorizontalHeaderLabels(["X","Y","I"])
        
        #create a layouts for buttons
        b_layout = QtGui.QGridLayout() #for buttons
        b_layout.addWidget(self.btn_open_camera,0,0)
        b_layout.addWidget(self.btn_snapshot,0,1)
        b_layout.addWidget(self.btn_add_roi,1,0)
        b_layout.addWidget(self.btn_move_rois,1,1)
        b_layout.addWidget(self.btn_project,2,0)
        b_layout.addWidget(self.btn_save_image,2,1)
        b_layout.addWidget(self.tbl,3,0,5,2)
        b_layout.addWidget(self.btn_record,8,0,1,2)
        
        #prepare space for snapped frame
        self.scene = QtGui.QGraphicsScene()
        self.view  = QtGui.QGraphicsView(self.scene)
        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(self.view)
        
        #set up main layout
        main_layout = QtGui.QGridLayout()
        main_layout.setColumnStretch(0,3)
        main_layout.setColumnStretch(3,8)
        
        #attach widgets and layouts
        main_layout.addLayout(b_layout,0,0)
        main_layout.addLayout(vbox,0,3)

        self.setLayout(main_layout)
        self.show()
    
    def __validate_files__(self):
        """Check for presence and validity of all dependencies"""
        if not os.path.exists("Outputfiles"):
            print "Creating Outputfiles sub dir...",
            os.mkdir("Outputfiles")
            print "Done!"
    
    def open_camera(self,pressed):
        source = self.sender()
        if not pressed:
            print "opening camera ..."
            self.video_player = VideoPlayer()
        else:
            print "camera closed"

    def snapshot(self,pressed):
        source = self.sender()
        #activate the mouse to detect every clicked location on the view pad
        self.view.mousePressEvent = self.get_mouse_loc_on_press
        #clean vertices and pre-selected regions
        self.vertices = []
        self.regions = []
        if not pressed:
            frame = self.video_player.camera.frame
            #save the snapped image
            self.snapped_image = np.asarray(frame[:,:]).copy()
            
            self.pix = QtGui.QPixmap(QtGui.QImage(frame.tostring(), frame.width, frame.height, QtGui.QImage.Format_RGB888).rgbSwapped())
            
            #clear screen and display new image
            self.scene.clear()
            self.scene.addPixmap(self.pix)
            print "scene position:", QtGui.QGraphicsPixmapItem(self.pix).pos()

    def add_roi(self,pressed):
        self.regions.append(np.array(self.vertices,np.int32))
        print self.vertices, "added to regions of interest"
        print "number or regions:", len(self.regions)
        self.vertices = []
    

    def get_mouse_loc_on_press(self, event ):
        position = event.pos()
        #position = (event.pos().x(), event.pos().y())
        #position = (event.pos().x() - self.view.pos().x(),
        #            event.pos().y() - self.view.pos().y())
        self.vertices.append([position.x()-50,position.y()-15])
        #draw point
        point = QtCore.QPointF(self.view.mapToScene(position))
        line = QtCore.QLineF(point,point)
        pen = QtGui.QPen(QtCore.Qt.red, 0, QtCore.Qt.SolidLine)
        pen.setWidth(5)
        self.view.scene().addLine(line,pen)
        #self.view.scene().addItem(QtGui.QGraphicsLineItem(QtCore.QLineF(point, point)))
        print "click location:",(position.x(),position.y())
    
    
    def move_rois(self):
        pass

    def project(self,pressed):
        source = self.sender()
        if not pressed:
            arr = self.snapped_image.copy()
            cv2.fillPoly(arr, self.regions, 1)
            cv2.imwrite("Outputfiles/projection.png",arr)
            print "projection saved on disc"
            os.system("python project_img_gen3mainloop.py 1")
        pass


    def record(self):
        source = self.sender()
        if not self._record_signal:
            print "recording ..."
            self._record_signal = True
            self.frame_counter = 0
            self.btn_record.setText('Stop')
            self.timer = QtCore.QTimer()
            self.timer.setInterval(int(self.time_interval*1000))
            self.timer.timeout.connect(self._save_image_)
            self.timer.start()

        else:
            self.timer.stop()
            self.btn_record.setText('Record')
            self._record_signal = False
            print "done recording!"

    def _save_image_(self):
        self.frame_counter+=1
        frame = np.asarray(self.video_player.camera.frame[:,:])
        filename = "Outputfiles/img_{0:04}.png".format(self.frame_counter)
        cv2.imwrite(filename,frame)
        print "frame saved as", filename

    def save_snapped(self):
        cv2.imwrite("Outputfiles/snapped_frame.png",self.snapped_image)
        print "image saved as Outputfiles/snapped_image.png"









def main():
    app = QtGui.QApplication(sys.argv)
    mainWindow = MainWindow()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
