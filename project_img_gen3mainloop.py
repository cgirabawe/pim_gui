'''
		||project_img||
summary:
*This set of functions ought to provide a means to project an image
onto any desired screen of a computer with python, pyglet, and all the other 
imported modules listed below installed on it. 

comment on notation:
*inputs and outputs will be specified and their type indicated by a string of characters within ()
in the description above each function. Consult this table if you are confused as to what they indicated: 
**natural numbers {1,2,..infinity}                          (N)
**positive integers {0,1,2,3...infinity}                    (Z+)
**real numbers                                              (R)

authors:
*Ian Hunter
last updated:
*10/23/16
last three updates:
*created
*made compatible with mac CocoaScreens
*eliminated need for findscreen
'''
import sys
import time
import pyglet


'''
function:
input:
output:
comments/usage advice:
'''

'''
init_screen_proj
input:(N) index indicating which screen you'd like to project to. Note that 1 is not necessarily the
primary monitor. stdout \in {0,1} indicated false or true as to whether you would like to see
output about the screens and running of the function.
output: a pyglet (window)
comments/usage advice:
Remember to close the window once you are done [i.e. call close_screen_proj]. Don't call this 
multiple times to the same monitor. When I have done this performance has been very strange
and hard to predict.
'''
def init_screen_proj(screeni,stdout):
    
    platform = pyglet.window.get_platform()
    display = platform.get_default_display()
    screens = display.get_screens()
    if stdout:
        print screens
    screen = screens[screeni]
    window = pyglet.window.Window()
    #window.on_draw = proj_blank(window) #by default print a blank screen
    window.set_fullscreen(screen=screen)
    return window

def proj_blank(window):
    window.clear()
'''
close_screen_proj
input:an open pyglet window you want closed
output:void()
comments/usage advice:
'''
def close_screen_proj(openwindow):
    openwindow.close()

def proj_img_now(window,img):
    def on_drawim():
        window.clear()
        img.blit(0,0)#should this be be a constant?
    window.on_draw = on_drawim


def proj_img_later(window,img,dt):
    def on_drawim():
        window.clear()
        img.blit(0,0)#should this be be a constant?
    def reproj(dt):
        window.on_draw = on_drawim
    pyglet.clock.schedule_once(reproj, dt)


screeni = int(sys.argv[1])
print screeni



#image1 = pyglet.resource.image('matthew-stone3.jpg')
#image2 = pyglet.resource.image('katsui1.jpg')
image1 = pyglet.resource.image('Outputfiles/projection.png')
image2 = pyglet.resource.image('Outputfiles/projection.png')


timeout=30
openwin=init_screen_proj(screeni,True)
proj_img_now(openwin,image1)
#proj_img_later(openwin,image2,timeout/3.0)
i = 0
def alternateimg(dt):
    global i
    if i%2==0:
        def on_drawim():
            openwin.clear()
            image1.blit(0,0)
    else:
        def on_drawim():
            openwin.clear()
            image2.blit(0,0)
    openwin.on_draw = on_drawim
    i=i+1




interval=0.25
pyglet.clock.schedule_interval(alternateimg, interval)

def exiter(dt):
    pyglet.app.exit()
pyglet.clock.schedule_once(exiter, timeout)

pyglet.app.run()
quit()
